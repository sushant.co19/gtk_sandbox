#ifndef SIMPLE_APPLICATION_H
#define SIMPLE_APPLICATION_H

#include <gtkmm.h>
#include <iostream>


class SimpleApplication : public Gtk::Application
{
 protected:
  SimpleApplication();

 public:
  static Glib::RefPtr<SimpleApplication> create();

 protected:
  void on_startup()  override;
  void on_activate() override;

  void on_test1();
  void on_test2();
  void on_test3( int value );
  void on_test4( Glib::ustring value );
  void on_test5( Glib::VariantBase value );
  void on_test5_wrap();
  void reset();
  void disable();

  void on_no_widget();

private:
  void create_window();
  void on_window_hide(Gtk::Window* window);
  void set_tooltip_recurse(Gtk::Widget* widget);

  Glib::RefPtr<Gtk::Builder> m_refBuilder;

  Glib::RefPtr<Gio::SimpleAction> m_test1;
  Glib::RefPtr<Gio::SimpleAction> m_test2;
  Glib::RefPtr<Gio::SimpleAction> m_test3;
  Glib::RefPtr<Gio::SimpleAction> m_test4;
  Glib::RefPtr<Gio::SimpleAction> m_test5;
  Glib::RefPtr<Gtk::Adjustment> m_adjustment0;
  Glib::RefPtr<Gio::SimpleAction> m_reset;
  Glib::RefPtr<Gio::SimpleAction> m_disable;
};

#endif // SIMPLE_APPLICATION_H

