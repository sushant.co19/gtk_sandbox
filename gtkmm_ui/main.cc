
#include "simple-application.h"

int main(int argc, char *argv[])
{

  auto application = SimpleApplication::create();

  const int status = application->run(argc, argv);
  return status;
}
