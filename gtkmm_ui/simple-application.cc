

#include "simple-application.h"

#include <glibmm/i18n.h>
#include <gtk/gtk.h>

#include <iostream>
#include <iomanip>

#include "simple-widgets.h"

// ------------------------



SimpleApplication::SimpleApplication()
  : Gtk::Application("org.tavmjong.simpleapplication",
                     Gio::APPLICATION_HANDLES_OPEN |
                     Gio::APPLICATION_CAN_OVERRIDE_APP_ID)
{
  Glib::set_application_name("Simple Application");
}

Glib::RefPtr<SimpleApplication> SimpleApplication::create()
{
  return Glib::RefPtr<SimpleApplication>(new SimpleApplication());
}

void
SimpleApplication::on_startup()
{
  Gtk::Application::on_startup();

  // Add actions
  Glib::VariantType Double(G_VARIANT_TYPE_DOUBLE);
  m_test1   = add_action(              "test1",          sigc::mem_fun(*this, &SimpleApplication::on_test1) );
  m_test2   = add_action_bool(         "test2",          sigc::mem_fun(*this, &SimpleApplication::on_test2), false);
  m_test3   = add_action_radio_integer("test3",          sigc::mem_fun(*this, &SimpleApplication::on_test3), 0);
  m_test4   = add_action_radio_string( "test4",          sigc::mem_fun(*this, &SimpleApplication::on_test4), "Mango");
  m_test5   = add_action_with_parameter("test5", Double, sigc::mem_fun(*this, &SimpleApplication::on_test5));
  m_reset   = add_action(              "reset",          sigc::mem_fun(*this, &SimpleApplication::reset) );
  m_disable = add_action_bool(         "disable",        sigc::mem_fun(*this, &SimpleApplication::disable));
  add_action(               "no_widget",     sigc::mem_fun(*this, &SimpleApplication::on_no_widget));


  // Custom widgets with proxyies must be instantiated at least once before they can be used!
  auto tmp = new SimpleButton2();
  //delete tmp;

  // Construct UI (see Gio::Menu to construct by hand)
  m_refBuilder = Gtk::Builder::create();

  try
    {
      m_refBuilder->add_from_file("simple-application.ui");
    }
  catch (const Glib::Error& err)
    {
      std::cerr << "Building UI failed: " << err.what();
    }

  // Add menu to application.
  auto object = m_refBuilder->get_object("menu-bar");
  auto menu = Glib::RefPtr<Gio::Menu>::cast_dynamic(object);
  if (!menu) {
    std::cerr << "Menu not found" << std::endl;
  } else {
    set_menubar( menu );  // Per window menus
  }


  // Manually add a simple button
  auto object2 = m_refBuilder->get_object("ToolbarD");
  auto toolbard = Glib::RefPtr<Gtk::Box>::cast_dynamic(object2);
  if (!toolbard) {
    std::cerr << "Toolbar D not found" << std::endl;
  } else {
    std::cout << "Toolbar D found!" << std::endl;
  }
  auto simple = Gtk::manage(new SimpleButton());
  simple->add_label("Manual Simple");
  gtk_actionable_set_action_name( GTK_ACTIONABLE(simple->gobj()), "app.test1");
  // simple->set_action_name("test1"); GTK4
  toolbard->add(*simple);

  // Get simple button from .ui file
  SimpleButton* simple1 = nullptr;
  m_refBuilder->get_widget_derived("SimpleButtonID", simple1);
  // auto simple1 = Gtk::Builder::get_widget_derived<SimpleButton>(m_refBuilder, "SimpleButton"); GTK4
  if (simple1) {
    std::cout << "Simple button from UI file found!" << std::endl;
  } else {
    std::cerr << "Simple button from UI file not found!" << std::endl;
  }

  SimpleButton2* simple2 = nullptr;
  m_refBuilder->get_widget_derived("SimpleButton2ID", simple2);
  if (simple2) {
    std::cout << "Simple button2 from UI file found!" << std::endl;
    std::cout << "Simple button2 property value: " << simple2->property_ustring() << std::endl;
  } else {
    std::cerr << "Simple button2 from UI file not found!" << std::endl;
  }
}

void
SimpleApplication::on_activate()
{
  std::cout << "SimpleApplication::on_activate()" << std::endl;
  create_window(); 
}

void SimpleApplication::on_test1()
{
  std::cout << "on_test1" << std::endl;
}

void SimpleApplication::on_test2()
{
  bool active = false;
  m_test2->get_state(active);
  active = !active;
  m_test2->change_state(active);

  if (active) {
    std::cout << "on_test2: active" << std::endl;
  } else {
    std::cout << "on_test2: not active" << std::endl;
  }
}

void SimpleApplication::on_test3( int value )
{
  static bool block = false;
  if (block) return;
  block = true;

  m_test3->change_state( value );

  block = false;

  std::cout << "on_test3: " << value << std::endl;
}

void SimpleApplication::on_test4( Glib::ustring value )
{
  static bool block = false;
  if (block) return;
  block = true;

  m_test4->change_state( value );

  block = false;

  std::cout << "on_test4: " << value << std::endl;
}

void SimpleApplication::on_test5( Glib::VariantBase value )
{
  // No state
  Glib::Variant<double> d = Glib::VariantBase::cast_dynamic<Glib::Variant<double> > (value);
  std::cout << "on_test5: " << d.get() << std::endl;
}

void SimpleApplication::on_test5_wrap()
{
  std::cout << "on_test5_wrap" << std::endl;
  double value = m_adjustment0->get_value();
  // Do some validiation...
  activate_action("test5", Glib::Variant<double>::create(value) );
}


// Reset to default values
void SimpleApplication::reset()
{
  std::cout << "SimpleApplication::reset(): Entrance" << std::endl;

  // We can directly change boolean values.
  m_test2->change_state(false);

  // We cannot directly change radio buttons as they trigger changes
  // the other buttons in the group which leads to loops. Activating
  // the action prevents this as the callback blocks additonal
  // callbacks.
  on_test3 (0);
  on_test4 ("Banana");
  //m_test3->change_state(2);
  std::cout << "SimpleApplication::reset(): Exit" << std::endl;
}


// Reset to default values
void SimpleApplication::disable()
{
  std::cout << "SimpleApplication::disable(): Entrance" << std::endl;

  bool active = false;
  m_disable->get_state(active);

  active = !active;

  m_disable->change_state(active);

  if (active) {
    std::cout << "disable: active" << std::endl;
  } else {
    std::cout << "disable: not active" << std::endl;
  }

  m_test1->set_enabled(active);
  m_test3->set_enabled(active);
}

void SimpleApplication::on_no_widget()
{
  std::cout << "SimpleApplication:on_no_widget" << std::endl;
}

void
SimpleApplication::create_window()
{
  std::cout << "SimpleApplication::create_window()" << std::endl;

#if 0
  // The menubar isn't loaded with this code!
  // Gtk::ApplicationWindow is derived from Gtk::Bin so it can contain only one child.
  Gtk::ApplicationWindow* application_window = nullptr;
  m_refBuilder->get_widget("ApplicationWindow", application_window);
  if (!application_window) {
    std::cerr << "Application window not found!" << std::endl;
    return;
  }

  //application_window->set_title("Simple Application");
#else
  auto application_window = new Gtk::ApplicationWindow();
  Gtk::Box* main_box = nullptr;
  m_refBuilder->get_widget("MainBox", main_box);
  if (!main_box) {
    std::cerr << "Application main box not found!" << std::endl;
    return;
  }
  main_box->unparent();
  application_window->add(*main_box);
#endif

  add_window( *application_window );  // Application runs till window is closed.
  application_window->signal_hide().connect(
    sigc::bind<Gtk::Window*>(sigc::mem_fun(*this, &SimpleApplication::on_window_hide), application_window)
  );

  // Styling --------------------------------------------------

  auto provider = Gtk::CssProvider::create();
  try {
    provider->load_from_path("simple-application.css");
  }
  catch (...) {
  }

  auto const screen = Gdk::Screen::get_default();
  Gtk::StyleContext::add_provider_for_screen (screen, provider, GTK_STYLE_PROVIDER_PRIORITY_USER);

  set_tooltip_recurse(application_window);

  std::vector<Glib::ustring> action_descriptions = list_action_descriptions();
  for (auto action_description : action_descriptions) {
    std::cout << "Action description: " << action_description;
    std::vector<Glib::ustring> accels = get_accels_for_action(action_description);
    for (auto accel : accels) {
      std::cout << "  " << accel;
    }
    std::cout << std::endl;
  }

  application_window->show_all();

  std::cout << "SimpleApplication::create_window(): Exit" << std::endl;
}

void
SimpleApplication::on_window_hide(Gtk::Window* window)
{
  std::cout << "SimpleApplication::on_window_hide()" << std::endl;
  delete window;
}

void
SimpleApplication::set_tooltip_recurse(Gtk::Widget* widget)
{
  static int indent = 0;
  indent++;

  Glib::ustring action_description;
  Glib::ustring action_accelerator;

  // Until GTK4 we need to fallback to GTK+
  if (GTK_IS_ACTIONABLE(widget->gobj())) {

    GtkActionable* actionable = GTK_ACTIONABLE(widget->gobj());
    const gchar* name = gtk_actionable_get_action_name (actionable);
    if (name) {
      action_description = name;
      GVariant* variant = gtk_actionable_get_action_target_value(actionable);
      if (variant) {

        if (       g_variant_is_of_type(variant, G_VARIANT_TYPE_INT32)) {
          const int integer = g_variant_get_int32 (variant);
          action_description += "(";
          action_description += std::to_string (integer);
          action_description += ")";

        } else if (g_variant_is_of_type(variant, G_VARIANT_TYPE_STRING)) {
          const gchar* string  = g_variant_get_string (variant, nullptr);
          action_description += "::";
          action_description += string;

        } else {
          std::cerr << "SimpleApplication::gui_recurse: unhandled variant type: "
                    << g_variant_print (variant, true) << std::endl;
        }

        std::vector<Glib::ustring> accels = get_accels_for_action(action_description);
        if (!accels.empty()) {
          action_accelerator = accels[0];

          Glib::ustring tooltip = widget->get_tooltip_text();
          // To do, strip off any old accelerator info. Format like AccelLabel.
          tooltip += " (" + action_accelerator + ")";
          widget->set_tooltip_text( tooltip );
        }
      }
    }
  }

  // Glib::Value<std::string> string;
  // string.init(G_TYPE_STRING); // Must init!!!!
  // button->get_property_value("action_name", string);
  // std::string action_name(string.get());
  // std::cout << " action_name: "   << action_name << std::endl;

  for (auto i = 0; i < indent; ++i) {
    std::cout << "  ";
  }

  std::cout << std::setw(30) << std::left << widget->get_name();

  for (int i = 8 - indent; i > 0; --i) {
    std::cout << "  ";
  }

  std::cout << "    " << std::setw(20) << std::left << action_description
            << "    " << std::setw(10) << std::left << action_accelerator
            << "    " << widget->get_tooltip_text() << std::endl;;


  auto container = dynamic_cast<Gtk::Container*>(widget);
  if (container) {
    auto children = container->get_children();
    for (auto child: children) {
      set_tooltip_recurse (child);
    }
  }

  indent--;
}
