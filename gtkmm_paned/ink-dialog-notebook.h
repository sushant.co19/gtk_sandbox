#ifndef INK_DIALOG_NOTEBOOK_H
#define INK_DIALOG_NOTEBOOK_H

/*
 * A wrapper for Gtk::Notebook.
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include <gtkmm.h>

/** A widget that wraps a Gtk::Notebook with dialogs as pages.
    (This could be derived from Gtk::Notebook if one doesn't need the bottom bar.)
    A notebook is fixed to a specific InkDialogContainer which manages the dialogs
    inside the notebook.
 */

class InkDialogContainer;

class InkDialogNotebook : public Gtk::Box
{
 public:
  InkDialogNotebook(InkDialogContainer* container);
  virtual ~InkDialogNotebook() {};

  Gtk::Notebook* get_notebook() { return &_notebook; }
  void add_page(Gtk::Widget& page, Glib::ustring label);
  void add_page(Gtk::Widget& page, Gtk::Widget& tab, Glib::ustring label);
  void move_page(Gtk::Widget& page);
  InkDialogContainer* get_container() { return _container;}

  // Signal handlers - Notebook
  void on_drag_begin  (const Glib::RefPtr<Gdk::DragContext> context);
  void on_drag_end    (const Glib::RefPtr<Gdk::DragContext> context);
  bool on_drag_failed (const Glib::RefPtr<Gdk::DragContext> context, Gtk::DragResult result);
  
  void on_page_added  (Gtk::Widget* page, int page_num);
  void on_page_removed(Gtk::Widget* page, int page_num);

  // Signal handlers - Notebook menu (action)
  void close_tab_callback();
  void hide_tab_label_callback();
  void show_tab_label_callback();
  void hide_all_tab_labels_callback();
  void show_all_tab_labels_callback();
  void move_tab_callback();
  void action_activate_callback(Glib::ustring dialog);

  // Signal handlers - Other
  void close_notebook_callback();
  bool close_notebook_button_callback(GdkEventButton* release_event);

private:
  InkDialogContainer* _container;

  //static bool _builder_built;
  //static Glib::RefPtr<Gtk::Builder> _builder;
  Glib::RefPtr<Gtk::Builder> _builder;

  Gtk::MenuButton _action_button;
  Gtk::Menu       _action_menu;
  Gtk::Notebook   _notebook;
  Gtk::Button     _close_button;
  Gtk::Box        _bar;

};

#endif // INK_DIALOG_NOTEBOOK_H
