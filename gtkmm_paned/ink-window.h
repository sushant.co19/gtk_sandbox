#ifndef INK_WINDOW_H
#define INK_WINDOW_H

#include <gtkmm.h>

class InkDialogContainer;
class InkMultipaned;
class InkDocument;

class InkWindow : public Gtk::ApplicationWindow
{
public:
  InkWindow(InkDocument* document);
  virtual ~InkWindow() {};

  void on_close();
  void on_new_dialog(Glib::ustring value);
  void on_action_change_tool(Glib::ustring value);
  void on_change_background(Glib::ustring value);
  void on_bool();

  InkDocument* get_document() { return document; };

private:
  Glib::RefPtr<Gtk::Builder> builder;
  Glib::RefPtr<Gio::SimpleAction> bool_action;

  InkDialogContainer* container;
  InkMultipaned* columns;

  Gtk::FlowBox* tools_flowbox;
  Gtk::Image* logo;

  InkDocument* document;
};

#endif //INK_WINDOW_H
