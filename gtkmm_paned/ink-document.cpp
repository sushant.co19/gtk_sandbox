
#include "ink-document.h"

#include <iostream>

InkDocument::InkDocument()
  : _color ("lightblue")
{
  _action_group = Gio::SimpleActionGroup::create();
  _bool_action = _action_group->add_action_bool("bool", sigc::mem_fun (*this, &InkDocument::on_bool));
}

void
InkDocument::on_bool()
{
  bool active = false;
  _bool_action->get_state (active);

  active = !active;
  _bool_action->change_state (active);

  std::cout << "InkDocument::on_bool: " << std::boolalpha << active << "  " << _color << std::endl;
}
