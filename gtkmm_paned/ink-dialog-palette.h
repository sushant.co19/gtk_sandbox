#ifndef INK_DIALOG_PALETTE_H
#define INK_DIALOG_PALETTE_H

/*
 * A palette dialog.
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include "ink-dialog-base.h"

class InkDialogPalette : public InkDialogBase
{
 public:
  InkDialogPalette(Glib::ustring name);
  virtual ~InkDialogPalette() override {};
  void init() override;

private:
  void on_click() override;

  int _swatch_size;
  void fill_color_names();
  std::vector<Glib::ustring> _color_names;
};

#endif // INK_DIALOG_PALETTE_H
