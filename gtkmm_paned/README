

A proof of principle for an Inkscape notebook based dialog manager featuring
a Gimp style multi-pane.

Dialog tabs can be added from main menu or from drop-down menu in a
notebook.

Dialog tabs can be dragged and dropped:
  * between notebooks,
  * to the left/right sides of a window,
  * to the top/bottom of a column of notebooks.

Dialogs will act on the document window they appear in or on
on the last document window the cursor was in if the dialog is
in a floating dialog window.


g++ *.cpp -g -o paned `pkg-config gtkmm-3.0 --cflags --libs`

# To run with debug window:
GTK_DEBUG=interactive ./paned

# To use CSS: in debug window, paste into CSS tab.


# To run with x11 backend
GDK_BACKEND=x11 ./paned

# To run with x11 backend
GDK_BACKEND=wayland ./paned


To do:

 * Better space allocation of panes.


Questions:

 * Why does drag and drop not work with x11 backend?

   To test:
    1. Start up the test program:
       > GDK_BACKEND=x11 ./paned

    2. Add a dialog (select one under the Menu "Dialogs" entry).
       The dialog will be added on the right side of the main window
       as a tab in a Gtk::Notebook widget.

    3. Drag the dialog tab to the right or left edge of the main
       window (alternatively, the top or bottom edge of the notebook
       or outside the main window).

       A 5 pixel wide drop-zone should be highlighted with a green
       stroke. Dropping in this zone (or outside the window) should
       create a new dialog Gtk::Notebook containing the dialog in a
       tab. This does not happen under X11.
