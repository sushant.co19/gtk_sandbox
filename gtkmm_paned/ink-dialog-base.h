#ifndef INK_DIALOG_BASE_H
#define INK_DIALOG_BASE_H

/*
 * A base class for all dialogs.
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include <gtkmm.h>

class InkDialogBase : public Gtk::Box
{
 public:
  InkDialogBase(Glib::ustring name);
  virtual ~InkDialogBase() {};
  virtual void init();
  virtual void update();
  void blink();

 private:
  bool blink_off(); // timer callback
  virtual void on_click();

 protected:
  std::string _name;         // Gtk widget name (must be set!).
  Gtk::Button _debug_button; // For printing to console.
};

#endif // INK_DIALOG_BASE_H
