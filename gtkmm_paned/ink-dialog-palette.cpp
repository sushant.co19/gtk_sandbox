
/*
 * A palette dialog.
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include "ink-dialog-palette.h"

#include <iostream>

class MyDrawingArea : public Gtk::DrawingArea
{
public:
  MyDrawingArea()
    : Gtk::DrawingArea()
  {}
  virtual ~MyDrawingArea() {}

  bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
  bool on_button_press_event(GdkEventButton *button_event) override;
  // bool on_event(GdkEvent* event) override;

  void set_color(Glib::ustring color) { m_color = color; };
  Glib::ustring get_color() { return m_color; };

private:
  Glib::ustring m_color;
};

bool
MyDrawingArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
  // std::cout << "MyDrawingArea::on_draw" << std::endl;
  Gdk::RGBA rgba(m_color);
  Gdk::Cairo::set_source_rgba(cr, rgba);
  cr->paint();
  return true;
}

// Button here refers to mouse button.
bool
MyDrawingArea::on_button_press_event(GdkEventButton *button_event) {
  std::cout << "on_button_press_event: " << get_name() << std::endl;
  return true;
}

// Button here refers to Gtk::Button
void
button_clicked_callback(MyDrawingArea *drawing_area) {
  std::cout << "button_clicked_callback: " << drawing_area->get_color() << std::endl;

  Gtk::Button* button = dynamic_cast<Gtk::Button*>(drawing_area->get_parent());
  if (!button) {
    std::cout << "  Did not find button!" << std::endl;
    return;
  }

  Gtk::FlowBoxChild* flowboxchild = dynamic_cast<Gtk::FlowBoxChild*>(button->get_parent());
  if (!flowboxchild) {
    std::cout << "  Did not get FlowBoxChild" << std::endl;
    return;
  }

  Gtk::FlowBox* flowbox = dynamic_cast<Gtk::FlowBox*>(flowboxchild->get_parent());
  if (!flowbox) {
    std::cout << "  Did not get FlowBox" << std::endl;
    return;
  }

  flowbox->select_child(*flowboxchild);
}

int
on_sort(Gtk::FlowBoxChild* a, Gtk::FlowBoxChild* b)
{
  Gdk::RGBA color_a(dynamic_cast<MyDrawingArea *>(dynamic_cast<Gtk::Button *>(a->get_child())->get_child())->get_color());
  Gdk::RGBA color_b(dynamic_cast<MyDrawingArea *>(dynamic_cast<Gtk::Button *>(b->get_child())->get_child())->get_color());
  return (color_a.get_red() - 0.5 * color_a.get_green() - 0.5 * color_a.get_blue() <
          color_b.get_red() - 0.5 * color_b.get_green() - 0.5 * color_b.get_blue() );
          
  // if (color_a.get_red() != color_b.get_red()) {
  //   return (color_a.get_red() < color_b.get_red());
  // } else if (color_a.get_green() != color_b.get_green()) {
  //   return (color_a.get_green() < color_b.get_green());
  // } else {
  //   return (color_a.get_blue() < color_b.get_blue());
  // }
};

// =============================================================

InkDialogPalette::InkDialogPalette(Glib::ustring name)
  : InkDialogBase(name)
{
  std::cout << "InkDialogPalette::InkDialogPalette: " << name << std::endl;
  set_name(name);

}

void
InkDialogPalette::init()
{
  _swatch_size = 10;

  set_orientation(Gtk::ORIENTATION_VERTICAL);

  // Flow box
  Gtk::FlowBox* flowbox = Gtk::manage(new Gtk::FlowBox());
  flowbox->set_max_children_per_line(200);
  flowbox->set_homogeneous();
  flowbox->set_row_spacing(0);
  flowbox->set_column_spacing(0);
  flowbox->set_hexpand(true);
  flowbox->set_vexpand(true);
  flowbox->set_sort_func(&on_sort);
  flowbox->set_selection_mode( Gtk::SELECTION_SINGLE );
  flowbox->set_valign( Gtk::ALIGN_START);

  // Scrolled window
  Gtk::ScrolledWindow* scrolledwindow = Gtk::manage(new Gtk::ScrolledWindow());
  scrolledwindow->add(*flowbox);
  scrolledwindow->set_policy( Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC);
  add(*scrolledwindow);

  // Add swatches
  fill_color_names();
  for (auto name : _color_names) {
    // std::cout << name << std::endl;
    auto drawing_area = Gtk::manage(new MyDrawingArea());
    drawing_area->set_color(name);
    drawing_area->set_size_request(_swatch_size, _swatch_size);

    auto button = Gtk::manage(new Gtk::Button());
    button->add( *drawing_area );
    flowbox->add(*button);

    gtk_actionable_set_action_name (GTK_ACTIONABLE(button->gobj()), "app.change_background");
    gtk_actionable_set_action_target_value (GTK_ACTIONABLE(button->gobj()),
                                            g_variant_new_string(name.c_str()));
    //    button->signal_clicked().connect(sigc::bind<MyDrawingArea *>(sigc::ptr_fun(&button_clicked_callback), drawing_area));
  }

  Gtk::Label* label = Gtk::manage(new Gtk::Label("Palette Chooser, Etc."));

  pack_end(*label, Gtk::PACK_SHRINK);

  show_all_children();
}

// Not used
void
InkDialogPalette::on_click()
{
  std::cout << "InkDialogPalette: on_click: " << _name << std::endl;
  // Warning: get_window() returns Gdk::Window, not Gtk::Window (which is a totally different beast).
  Gtk::Widget* window = get_toplevel();
  if (window) {
    std::cout << "Dialog is part of: " << window->get_name() << std::endl;
  } else {
    std::cerr << "InkDialogPalette::on_click(): Dialog not attached to window!" << std::endl;
  }
}

void
InkDialogPalette::fill_color_names()
{
  _color_names.push_back("AliceBlue");
  _color_names.push_back("AntiqueWhite");
  _color_names.push_back("AntiqueWhite1");
  _color_names.push_back("AntiqueWhite2");
  _color_names.push_back("AntiqueWhite3");
  _color_names.push_back("AntiqueWhite4");
  _color_names.push_back("aqua");
  _color_names.push_back("aquamarine");
  _color_names.push_back("aquamarine1");
  _color_names.push_back("aquamarine2");
  _color_names.push_back("aquamarine3");
  _color_names.push_back("aquamarine4");
  _color_names.push_back("azure");
  _color_names.push_back("azure1");
  _color_names.push_back("azure2");
  _color_names.push_back("azure3");
  _color_names.push_back("azure4");
  _color_names.push_back("beige");
  _color_names.push_back("bisque");
  _color_names.push_back("bisque1");
  _color_names.push_back("bisque2");
  _color_names.push_back("bisque3");
  _color_names.push_back("bisque4");
  _color_names.push_back("black");
  _color_names.push_back("BlanchedAlmond");
  _color_names.push_back("blue");
  _color_names.push_back("blue1");
  _color_names.push_back("blue2");
  _color_names.push_back("blue3");
  _color_names.push_back("blue4");
  _color_names.push_back("BlueViolet");
  _color_names.push_back("brown");
  _color_names.push_back("brown1");
  _color_names.push_back("brown2");
  _color_names.push_back("brown3");
  _color_names.push_back("brown4");
  _color_names.push_back("burlywood");
  _color_names.push_back("burlywood1");
  _color_names.push_back("burlywood2");
  _color_names.push_back("burlywood3");
  _color_names.push_back("burlywood4");
  _color_names.push_back("CadetBlue");
  _color_names.push_back("CadetBlue1");
  _color_names.push_back("CadetBlue2");
  _color_names.push_back("CadetBlue3");
  _color_names.push_back("CadetBlue4");
  _color_names.push_back("chartreuse");
  _color_names.push_back("chartreuse1");
  _color_names.push_back("chartreuse2");
  _color_names.push_back("chartreuse3");
  _color_names.push_back("chartreuse4");
  _color_names.push_back("chocolate");
  _color_names.push_back("chocolate1");
  _color_names.push_back("chocolate2");
  _color_names.push_back("chocolate3");
  _color_names.push_back("chocolate4");
  _color_names.push_back("coral");
  _color_names.push_back("coral1");
  _color_names.push_back("coral2");
  _color_names.push_back("coral3");
  _color_names.push_back("coral4");
  _color_names.push_back("CornflowerBlue");
  _color_names.push_back("cornsilk");
  _color_names.push_back("cornsilk1");
  _color_names.push_back("cornsilk2");
  _color_names.push_back("cornsilk3");
  _color_names.push_back("cornsilk4");
  _color_names.push_back("crimson");
  _color_names.push_back("cyan");
  _color_names.push_back("cyan1");
  _color_names.push_back("cyan2");
  _color_names.push_back("cyan3");
  _color_names.push_back("cyan4");
  _color_names.push_back("DarkBlue");
  _color_names.push_back("DarkCyan");
  _color_names.push_back("DarkGoldenrod");
  _color_names.push_back("DarkGoldenrod1");
  _color_names.push_back("DarkGoldenrod2");
  _color_names.push_back("DarkGoldenrod3");
  _color_names.push_back("DarkGoldenrod4");
  _color_names.push_back("DarkGray");
  _color_names.push_back("DarkGreen");
  _color_names.push_back("DarkGrey");
  _color_names.push_back("DarkKhaki");
  _color_names.push_back("DarkMagenta");
  _color_names.push_back("DarkOliveGreen");
  _color_names.push_back("DarkOliveGreen1");
  _color_names.push_back("DarkOliveGreen2");
  _color_names.push_back("DarkOliveGreen3");
  _color_names.push_back("DarkOliveGreen4");
  _color_names.push_back("DarkOrange");
  _color_names.push_back("DarkOrange1");
  _color_names.push_back("DarkOrange2");
  _color_names.push_back("DarkOrange3");
  _color_names.push_back("DarkOrange4");
  _color_names.push_back("DarkOrchid");
  _color_names.push_back("DarkOrchid1");
  _color_names.push_back("DarkOrchid2");
  _color_names.push_back("DarkOrchid3");
  _color_names.push_back("DarkOrchid4");
  _color_names.push_back("DarkRed");
  _color_names.push_back("DarkSalmon");
  _color_names.push_back("DarkSeaGreen");
  _color_names.push_back("DarkSeaGreen1");
}
