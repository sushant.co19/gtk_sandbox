#ifndef MY_MULTIPANED_H
#define MY_MULTIPANED_H

#define USE_GESTURES

/*
 * A widget with multiple panes. Agnotic to type what kind of widgets panes contain.
 *
 * Copyright (C) 2020 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include <gtkmm.h>

/** A widget with multiple panes */

class MyHandle;

class MyMultipaned : public Gtk::Container, public Gtk::Orientable
{
 public:
  MyMultipaned(Gtk::Orientation orientation=Gtk::ORIENTATION_HORIZONTAL);
  virtual ~MyMultipaned();

  void prepend(Gtk::Widget* new_widget);
  void append( Gtk::Widget* new_widget);

protected:

  // Overrides
  Gtk::SizeRequestMode get_request_mode_vfunc() const override;
  void get_preferred_width_vfunc(int& minimum_width, int& natural_width) const override;
  void get_preferred_height_vfunc(int& minimum_height, int& natural_height) const override;
  void get_preferred_width_for_height_vfunc(int height, int& minimum_width, int& natural_width) const override;
  void get_preferred_height_for_width_vfunc(int width, int& minimum_height, int& natural_height) const override;
  void on_size_allocate(Gtk::Allocation& allocation) override;

  // Allow us to keep track of our widgets ourselves.
  void forall_vfunc(gboolean include_internals, GtkCallback callback, gpointer callback_data) override;

  void on_add(Gtk::Widget* child) override;
  void on_remove(Gtk::Widget* child) override;

private:

  void dump();

  // We must manage children ourselves.
  std::vector<Gtk::Widget *> children;

  // Until we figure out how to get Gtk::Orientable to work!
  Gtk::Orientation _orientation = Gtk::ORIENTATION_HORIZONTAL;

  // Values used when dragging handle.
  int handle            = -1; // Child number of active handle
  Gtk::Allocation start_allocation1;
  Gtk::Allocation start_allocationh;
  Gtk::Allocation start_allocation2;
  Gtk::Allocation allocation1;
  Gtk::Allocation allocationh;
  Gtk::Allocation allocation2;

  int  next_i = 0; // Vary which widget get first excess.

#ifdef USE_GESTURES
  Glib::RefPtr<Gtk::GestureDrag> gesture;
  void on_drag_begin  (double start_x,  double start_y );
  void on_drag_end    (double offset_x, double offset_y);
  void on_drag_update (double offset_x, double offset_y);
#else
  bool on_button_press_event   (GdkEventButton* button_event) override;
  bool on_button_release_event (GdkEventButton* button_event) override;
  bool on_motion_notify_event  (GdkEventMotion* motion_event) override;
  int start_x = -1;
  int start_y = -1;
#endif
};


#endif
