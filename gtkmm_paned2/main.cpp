
#include "my-application.h"

int main(int argc, char *argv[])
{
    auto application = MyApplication::create();

    return application->run(argc, argv);
}
