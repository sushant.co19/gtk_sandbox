/*
 * A widget with multiple panes. Agnotic to type what kind of widgets panes contain.
 * Handles allow a user to resize children widgets. Drop zones allow adding widgets
 * at either end.
 *
 * Copyright (C) 2020 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include <iostream>
#include <iomanip>

#include "my-multipaned.h"

/*
 * References:
 *   https://blog.gtk.org/2017/06/
 *   https://developer.gnome.org/gtkmm-tutorial/stable/sec-custom-containers.html.en
 *   https://wiki.gnome.org/HowDoI/Gestures
 *
 * The children widget sizes are "sticky". They change a minimal
 * amount when the parent widget is resized or a child is added or
 * removed.
 *
 * A gesture is used to track handle movement. This must be attached
 * to the parent widget (the offset_x/offset_y values are relative to
 * the widget allocation which changes for the handles as they are
 * moved).
 */

/* ============ DROPZONE  ============ */

class MyDropZone : public Gtk::EventBox
{
public:
  MyDropZone(Gtk::Orientation orientation);
  virtual ~MyDropZone() = default;

  // bool on_focus_in_event(GdkEventFocus* focus_event) override;

  // bool on_enter_notify(GdkEventCrossing* crossing_event);
  // bool on_leave_notify(GdkEventCrossing* crossing_event);

  // void on_drag_begin  (double start_x,  double start_y );
  // void on_drag_end    (double offset_x, double offset_y);
  // void on_drag_update (double offset_x, double offset_y);

  // void set_position   (double offset_x, double offset_y);

  // Glib::RefPtr<Gtk::GestureDrag> gesture;

private:
  Gtk::Orientation _orientation;

  MyMultipaned* _parent = nullptr;
};

MyDropZone::MyDropZone(Gtk::Orientation orientation)
  : Gtk::EventBox()
  , _orientation (orientation)
{
  set_name ("MyDropZone");
  set_has_window (false); // Default?

  // TEMP
  Gtk::Label* label = nullptr;
  if (_orientation == Gtk::ORIENTATION_HORIZONTAL) {
    label = Gtk::manage (new Gtk::Label("H"));
  } else {
    label = Gtk::manage (new Gtk::Label("V"));
  }
  add (*label);

  if (_orientation == Gtk::ORIENTATION_HORIZONTAL) {
    set_size_request(20, -1);
  } else {
    set_size_request(-1, 20);
  }

  set_visible_window(true);
  set_above_child(true); // Actually, no child.
}


/* ============  HANDLE   ============ */

class MyHandle : public Gtk::EventBox
{
public:
  MyHandle(Gtk::Orientation orientation);
  virtual ~MyHandle() = default;

  bool on_enter_notify_event(GdkEventCrossing* crossing_event) override;

private:
  Gtk::Orientation _orientation;
};

MyHandle::MyHandle(Gtk::Orientation orientation)
  : Gtk::EventBox()
  , _orientation (orientation)
{
  set_name ("MyHandle");

  // TEMP
  Gtk::Label* label = nullptr;
  if (_orientation == Gtk::ORIENTATION_HORIZONTAL) {
    label = Gtk::manage (new Gtk::Label("|"));
  } else {
    label = Gtk::manage (new Gtk::Label("-"));
  }
  add (*label);

  if (_orientation == Gtk::ORIENTATION_HORIZONTAL) {
    set_size_request(20, -1);
  } else {
    set_size_request(-1, 20);
  }

  set_visible_window(true);
  set_above_child(true); // Actually, no child.
}

bool
MyHandle::on_enter_notify_event(GdkEventCrossing* crossing_event)
{
  auto window = get_window();
  auto display = get_display();

  if (_orientation == Gtk::ORIENTATION_HORIZONTAL) {
    auto cursor = Gdk::Cursor::create(display, "col-resize");
    window->set_cursor (cursor);
  } else {
    auto cursor = Gdk::Cursor::create(display, "row-resize");
    window->set_cursor (cursor);
  }

  return false;
}

/* ============ MULTIPANE ============ */

MyMultipaned::MyMultipaned(Gtk::Orientation orientation)
  : Gtk::Container()
{
  set_name("MyMultipaned");

  // What magic is needed to get Gtk::Orientable to work?
  // Gtk::Orientable::add_interface(Glib::Interface::get_type());
  // set_orientation (orientation);
  _orientation = orientation; // Temp (I hope!)
  set_has_window (false);
  set_redraw_on_allocate (false);

  //  set_vexpand();
  //  set_hexpand();

  MyDropZone* dropzone_s = Gtk::manage (new MyDropZone(_orientation));
  MyDropZone* dropzone_e = Gtk::manage (new MyDropZone(_orientation));

  dropzone_s->set_parent(*this);
  dropzone_e->set_parent(*this);

  children.push_back(dropzone_s);
  children.push_back(dropzone_e);

#ifdef USE_GESTURES  
  gesture = Gtk::GestureDrag::create(*this);
  gesture->signal_drag_begin( ).connect(sigc::mem_fun(*this, &MyMultipaned::on_drag_begin ));
  gesture->signal_drag_end(   ).connect(sigc::mem_fun(*this, &MyMultipaned::on_drag_end   ));
  gesture->signal_drag_update().connect(sigc::mem_fun(*this, &MyMultipaned::on_drag_update));
#endif
}

MyMultipaned::~MyMultipaned()
{
  for (auto child : children) {
    if (child) child->unparent();
  }
}

void
MyMultipaned::prepend (Gtk::Widget* child)
{
  if (child) {

    // Add handle
    if (children.size() > 2) {
      MyHandle* handle = Gtk::manage (new MyHandle(_orientation));
      handle->set_parent(*this);
      children.insert(children.begin() + 1, handle); // After start dropzone
    }

    // Add child
    children.insert(children.begin() + 1, child);
    child->set_parent(*this);
  }
}

void
MyMultipaned::append (Gtk::Widget* child)
{
  if (child) {

    // Add handle
    if (children.size() > 2) {
      MyHandle* handle = Gtk::manage (new MyHandle(_orientation));
      handle->set_parent(*this);
      children.insert(children.end() - 1, handle); // Before end dropzone
    }

    // Add child
    child->set_parent(*this);
    children.insert(children.end() - 1, child);
  }
}

void
MyMultipaned::dump ()
{
  int i = 0;
  for (auto child: get_children()) {
    std::cout << " " << i++;
    if (auto label = dynamic_cast<Gtk::Label*>(child)) {
      std::cout << "  " << label->get_text();
    }
    std::cout << std::endl;
  }
}

// ****************** OVERRIDES ******************


Gtk::SizeRequestMode
MyMultipaned::get_request_mode_vfunc() const
{
  //  if (get_orientation() == Gtk::ORIENTATION_HORIZONTAL) {
  if (_orientation == Gtk::ORIENTATION_HORIZONTAL) {
    return Gtk::SIZE_REQUEST_WIDTH_FOR_HEIGHT;
  } else {
    return Gtk::SIZE_REQUEST_HEIGHT_FOR_WIDTH;
  }
}

void
MyMultipaned::get_preferred_width_vfunc(int& minimum_width, int& natural_width) const
{
  minimum_width = 0;
  natural_width = 0;
  for (auto child : children) {
    if (child && child->is_visible()) {
      int child_minimum_width = 0;
      int child_natural_width = 0;
      child->get_preferred_width(child_minimum_width, child_natural_width);
      if (_orientation == Gtk::ORIENTATION_VERTICAL) {
        minimum_width = std::max (minimum_width, child_minimum_width);
        natural_width = std::max (natural_width, child_natural_width);
      } else {
        minimum_width += child_minimum_width;
        natural_width += child_natural_width;
      }
    }
  }
}

void
MyMultipaned::get_preferred_height_vfunc(int& minimum_height, int& natural_height) const
{
  minimum_height = 0;
  natural_height = 0;
  for (auto child : children) {
    if (child && child->is_visible()) {
      int child_minimum_height = 0;
      int child_natural_height = 0;
      child->get_preferred_height(child_minimum_height, child_natural_height);
      if (_orientation == Gtk::ORIENTATION_HORIZONTAL) {
        minimum_height = std::max (minimum_height, child_minimum_height);
        natural_height = std::max (natural_height, child_natural_height);
      } else {
        minimum_height += child_minimum_height;
        natural_height += child_natural_height;
      }
    }
  }
}

void
MyMultipaned::get_preferred_width_for_height_vfunc(int height, int& minimum_width, int& natural_width) const
{
  minimum_width = 0;
  natural_width = 0;
  for (auto child : children) {
    if (child && child->is_visible()) {
      int child_minimum_width = 0;
      int child_natural_width = 0;
      child->get_preferred_width_for_height(height, child_minimum_width, child_natural_width);
      if (_orientation == Gtk::ORIENTATION_VERTICAL) {
        minimum_width = std::max (minimum_width, child_minimum_width);
        natural_width = std::max (natural_width, child_natural_width);
      } else {
        minimum_width += child_minimum_width;
        natural_width += child_natural_width;
      }
    }
  }
}

void
MyMultipaned::get_preferred_height_for_width_vfunc(int width, int& minimum_height, int& natural_height) const
{
  minimum_height = 0;
  natural_height = 0;
  for (auto child : children) {
    if (child && child->is_visible()) {
      int child_minimum_height = 0;
      int child_natural_height = 0;
      child->get_preferred_height_for_width(width, child_minimum_height, child_natural_height);
      if (_orientation == Gtk::ORIENTATION_HORIZONTAL) {
        minimum_height = std::max (minimum_height, child_minimum_height);
        natural_height = std::max (natural_height, child_natural_height);
      } else {
        minimum_height += child_minimum_height;
        natural_height += child_natural_height;
      }
    }
  }
}

// Natural width: The width the widget really wants.
// Minimum width: The minimum width for a widget to be useful.
// Minimum <= Natural.
void
MyMultipaned::on_size_allocate(Gtk::Allocation& allocation)
{
  // std::cout << "\nMyMultipaned::on_size_allocate:"
  //           << " width: "  << allocation.get_width()
  //           << " height: " << allocation.get_height() << std::endl;

  set_allocation (allocation);

  // Avoid writing this out all the time!
  bool horizontal = _orientation == Gtk::ORIENTATION_HORIZONTAL;

  std::vector<int>  deltas (children.size(), 0); // change to apply to allocation.

  if (handle != -1) {

    // Exchange allocation between the widgets on either side of moved handle..
    // Allocation values calculated in on_drag_update();
    children[handle-1]->size_allocate(allocation1);
    children[handle  ]->size_allocate(allocationh);
    children[handle+1]->size_allocate(allocation2);

    // std::cout << " A: "
    //           << std::setw(4) << allocation1.get_x()     << " + "
    //           << std::setw(4) << allocation1.get_width() << " = "
    //           << std::setw(4) << allocationh.get_x()     << " + "
    //           << std::setw(4) << allocationh.get_width() << " = "
    //           << std::setw(4) << allocation2.get_x()     << " + "
    //           << std::setw(4) << allocation2.get_width() << " = "
    //           << std::setw(4) << allocation2.get_x() + allocation2.get_width()
    //           << std::endl;

  } else {

    // This brance on first call, on window changing size, on adding or deleting a widget.

    // Track difference between parent allocation and sum of children.
    int delta_total = horizontal ? allocation.get_width() : allocation.get_height();

    // Some things to track so we don't need to keep finding them.
    std::vector<bool> visibles;       // Is child visible?
    std::vector<bool> expandables;    // Is child expandable?
    std::vector<int>  delta_minimums; // Difference between allocated space and minimum space.
    std::vector<int>  delta_naturals; // Difference between allocated space and natural space.
    int visible_children = 0;
    int expandable_children = 0;

    // First loop to find surplus (or deficit).
    for (auto child : children) {

      bool visible = child->get_visible();
      bool expandable = child->compute_expand(_orientation);

      visibles.push_back(visible);
      expandables.push_back(expandable);

      int delta_minimum = 0;
      int delta_natural = 0;

      if (visible) {
        visible_children++;

        Gtk::Requisition req_minimum;
        Gtk::Requisition req_natural;
        child->get_preferred_size(req_minimum, req_natural);
        Gtk::Allocation child_allocation = child->get_allocation();

        if (horizontal) {
          delta_minimum = child_allocation.get_width()  - req_minimum.width;
          delta_natural = child_allocation.get_width()  - req_natural.width;
        } else {
          delta_minimum = child_allocation.get_height() - req_minimum.height;
          delta_natural = child_allocation.get_height() - req_natural.height;
        }

        delta_minimums.push_back(delta_minimum);
        delta_naturals.push_back(delta_natural);

        delta_total -= horizontal ? child_allocation.get_width() : child_allocation.get_height();

        if (expandable) expandable_children++;
      }
    }

    if (visible_children == 0) {
      return; // Nothing to show!
    }

    // Second loop to allocate extra space.
    if (delta_total == 0) {
      // We can't just return here as we need to update perpendicular direction!

    } else if (delta_total > 0) {
      // We need to allocate extra space. Steps:
      // 1. Allocate to satisfy minimums (should in principle never happen).
      // 2. Allocate to satisfy naturals (if extra space available).
      // 3. Allocate rest to expandable children.

      // Allocate to satisfy minimum size.
      while (delta_total > 0) {

        // Find smallest (absolute) minimum of those that need space.
        int minimum = -1000000;
        int deficit_count = 0;
        int i = 0;
        for (auto child : children) {

          if (delta_minimums[i] < 0) {
            deficit_count++;
            if (delta_minimums[i] > minimum) {
              minimum = delta_minimums[i];
            }
          }
          i++;
        }

        // Break if nobody needs more to satisfy minimum size.
        if (deficit_count == 0) break;

        // Break if we don't have enough space.
        if (deficit_count * -minimum > delta_total) break;

        // Add minimum amount to all that need it.
        i = 0;
        for (auto child : children) {
          if (delta_minimums[i] < 0) {
            delta_minimums[i] -= minimum; // minimum is negative.
            delta_naturals[i] -= minimum;
            delta_total       += minimum;
            deltas[i]         -= minimum;
          }
          i++;
        }
      }

      // Allocate to satisfy natural size.
      while (delta_total > 0) {

        // Find smallest (absolute) minimum of those that need space.
        int minimum = -1000000;
        int deficit_count = 0;
        int i = 0;
        for (auto child : children) {

          if (delta_naturals[i] < 0) {
            deficit_count++;
            if (delta_naturals[i] > minimum) {
              minimum = delta_naturals[i];
            }
          }
          i++;
        }

        // Break if nobody needs more to satisfy natural size.
        if (deficit_count == 0) break;

        // Break if we don't have enough space.
        if (deficit_count * -minimum > delta_total) break;

        // Add minimum amount to all that need it.
        i = 0;
        for (auto child : children) {
          if (delta_naturals[i] < 0) {
            delta_naturals[i] -= minimum;
            delta_total       += minimum;
            deltas[i]         -= minimum;
          }
          i++;
        }
      }

      // Divy up remaining space to all who want it.
      if (expandables.size() > 0) {

        while (delta_total > 0) {

          int i = next_i; // Prevents all the excess from accumulating on first widgets.
          while (delta_total > 0) {
            for (auto child : children) {
              if (i >= children.size()) {
                i = 0;
              }
              if (expandables[i]) {
                deltas[i]++;
                delta_total--;
                if (delta_total == 0) {
                  next_i = i + 1;
                  break;
                }
              }
              i++;
            }
          }
        }
      }

    } else {
      // We need to recover space. Steps:
      // 1. Take space from children with size greater than natural size.
      // 2. Take space from children with size greater than minimum size.
      // Never go below minimum size.

      // Take space above natural space.
      int i = next_i;
      while (delta_total < 0) {
        bool do_break = true;
        for (auto child : children) {
          if (delta_total == 0) break;

          if (i >= children.size()) {
            i = 0;
          }
          if (delta_naturals[i] > 0) {
            delta_naturals[i]--;
            delta_minimums[i]--;
            deltas[i]--;
            delta_total++;
            do_break = false;
          }
          i++;
        }

        if (do_break) {
          // No more children that can give up space!
          break;
        }
      }

      // Take space above minimum space.
      while (delta_total < 0) {
        bool do_break = true;
        for (auto child : children) {
          if (delta_total == 0) break;

          if (i >= children.size()) {
            i = 0;
          }
          if (delta_minimums[i] > 0) {
            delta_minimums[i]--;
            deltas[i]--;
            delta_total++;
            do_break = false;
          }
          i++;
        }

        if (do_break) {
          // No more children that can give up space!
          break;
        }
      }
      next_i = i + 1;
    }

    // Set x and y values of allocations (widths should be correct).
    int current_x = allocation.get_x(); // Start with parent geometry.
    int current_y = allocation.get_y();

    int i = 0;
    for (auto child : children) {

      Gtk::Allocation child_allocation = child->get_allocation();

      child_allocation.set_x(current_x);
      child_allocation.set_y(current_y);

      if (horizontal) {
        int width = child_allocation.get_width();
        width += deltas[i];
        child_allocation.set_width (width);
        current_x += width;
        child_allocation.set_height (allocation.get_height());
      } else {
        int height = child_allocation.get_height();
        height += deltas[i];
        child_allocation.set_height (height);
        current_y += height;

        child_allocation.set_width (allocation.get_width());
      }
      child->size_allocate(child_allocation);
      i++;
    }
  }
}

void
MyMultipaned::forall_vfunc (gboolean, GtkCallback callback, gpointer callback_data)
{
  for (auto child : children) {
    callback(child->gobj(), callback_data);
  }
}

void
MyMultipaned::on_add (Gtk::Widget *child)
{
  if (child) {
    append (child);
  }
}

// TODO: Delete handle from children list.
void
MyMultipaned::on_remove (Gtk::Widget *child)
{
  if (child) {
    auto it = std::find (children.begin(), children.end(), child);
    if (it != children.end()) {
      const bool visible = child->get_visible();
      child->unparent();
      children.erase (it);
      if (visible) {
        queue_resize();
      }
    }
  }
}

#ifdef USE_GESTURES
void
MyMultipaned::on_drag_begin  (double start_x,  double start_y )
{
  // We clicked on handle.
  bool found = false;
  int child_number = 0;
  Gtk::Allocation allocation = get_allocation();
  for (auto child : get_children()) {
    MyHandle* handle = dynamic_cast<MyHandle *>(child);
    if (handle) {
      Gtk::Allocation child_allocation = handle->get_allocation();

      // Did drag start in handle?
      int x = child_allocation.get_x() - allocation.get_x();
      int y = child_allocation.get_y() - allocation.get_y();
      if (x < start_x && start_x < x + child_allocation.get_width() &&
          y < start_y && start_y < y + child_allocation.get_height()) {
        found = true;
        break;
      }
    }
    ++child_number;
  }


  if (!found) {
    gesture->set_state (Gtk::EVENT_SEQUENCE_DENIED);
    return;
  }

  if (child_number < 1 || child_number > get_children().size() - 2) {
    std::cerr << "MyMultipaned::on_drag_begin: Invalid child (" << child_number << "!!" << std::endl;
    gesture->set_state (Gtk::EVENT_SEQUENCE_DENIED);
    return;
  }

  gesture->set_state (Gtk::EVENT_SEQUENCE_CLAIMED);

  // Save for use in on_drag_update().
  handle = child_number;
  start_allocation1 = children[handle-1]->get_allocation();
  start_allocationh = children[handle  ]->get_allocation();
  start_allocation2 = children[handle+1]->get_allocation();
}

void
MyMultipaned::on_drag_end    (double offset_x, double offset_y)
{
  gesture->set_state (Gtk::EVENT_SEQUENCE_DENIED);
  handle = -1;
}

void
MyMultipaned::on_drag_update (double offset_x, double offset_y)
{
  allocation1 = children[handle-1]->get_allocation();
  allocationh = children[handle  ]->get_allocation();
  allocation2 = children[handle+1]->get_allocation();

  if (_orientation == Gtk::ORIENTATION_HORIZONTAL) {
    if (-offset_x > start_allocation1.get_width()) offset_x = -start_allocation1.get_width();
    if ( offset_x > start_allocation2.get_width()) offset_x =  start_allocation2.get_width();

    allocation1.set_width(start_allocation1.get_width() + offset_x);
    allocationh.set_x(    start_allocationh.get_x()     + offset_x);
    allocation2.set_x(    start_allocation2.get_x()     + offset_x);
    allocation2.set_width(start_allocation2.get_width() - offset_x);

    // std::cout << " U: "
    //           << std::setw(4) << allocation1.get_x()     << " + "
    //           << std::setw(4) << allocation1.get_width() << " = "
    //           << std::setw(4) << allocationh.get_x()     << " + "
    //           << std::setw(4) << allocationh.get_width() << " = "
    //           << std::setw(4) << allocation2.get_x()     << " + "
    //           << std::setw(4) << allocation2.get_width() << " = "
    //           << std::setw(4) << allocation2.get_x() + allocation2.get_width()
    //           << std::endl;

  } else {

    if (-offset_y > start_allocation1.get_height()) offset_y = -start_allocation1.get_height();
    if ( offset_y > start_allocation2.get_height()) offset_y =  start_allocation2.get_height();

    allocation1.set_height(start_allocation1.get_height() + offset_y);
    allocationh.set_y(     start_allocationh.get_y()      + offset_y);
    allocation2.set_y(     start_allocation2.get_y()      + offset_y);
    allocation2.set_height(start_allocation2.get_height() - offset_y);
  }

  queue_allocate(); // Relayout MyMultipaned content.
}

#else // USE_GESTURES

bool
MyMultipaned::on_button_press_event   (GdkEventButton* button_event)
{
  start_x = button_event->x_root;
  start_y = button_event->y_root;

    // We clicked on handle.
  bool found = false;
  int child_number = 0;
  Gtk::Allocation allocation = get_allocation();
  for (auto child : get_children()) {
    MyHandle* handle = dynamic_cast<MyHandle *>(child);
    if (handle) {
      Gtk::Allocation child_allocation = handle->get_allocation();

      // Did drag start in handle?
      int x = child_allocation.get_x();
      int y = child_allocation.get_y();
      if (x < start_x && start_x < x + child_allocation.get_width() &&
          y < start_y && start_y < y + child_allocation.get_height()) {
        found = true;
        break;
      }
    }
    ++child_number;
  }

  if (found) {
    // Save for use in on_motion_notify_event().
    handle = child_number;
    start_allocation1 = children[handle-1]->get_allocation();
    start_allocationh = children[handle  ]->get_allocation();
    start_allocation2 = children[handle+1]->get_allocation();
    return true;
  }

  return false;
}

bool
MyMultipaned::on_button_release_event (GdkEventButton* button_event)
{
  handle = -1;
  return true;
}

bool
MyMultipaned::on_motion_notify_event  (GdkEventMotion* motion_event)
{
  double offset_x = motion_event->x_root - start_x;
  double offset_y = motion_event->y_root - start_y;

  allocation1 = children[handle-1]->get_allocation();
  allocationh = children[handle  ]->get_allocation();
  allocation2 = children[handle+1]->get_allocation();

  if (_orientation == Gtk::ORIENTATION_HORIZONTAL) {

    if (-offset_x > start_allocation1.get_width()) offset_x = -start_allocation1.get_width();
    if ( offset_x > start_allocation2.get_width()) offset_x =  start_allocation2.get_width();

    allocation1.set_width(start_allocation1.get_width() + offset_x);
    allocationh.set_x(    start_allocationh.get_x()     + offset_x);
    allocation2.set_x(    start_allocation2.get_x()     + offset_x);
    allocation2.set_width(start_allocation2.get_width() - offset_x);

    // std::cout << " M: "
    //           << std::setw(4) << allocation1.get_x()     << " + "
    //           << std::setw(4) << allocation1.get_width() << " = "
    //           << std::setw(4) << allocationh.get_x()     << " + "
    //           << std::setw(4) << allocationh.get_width() << " = "
    //           << std::setw(4) << allocation2.get_x()     << " + "
    //           << std::setw(4) << allocation2.get_width() << " = "
    //           << std::setw(4) << allocation2.get_x() + allocation2.get_width()
    //           << std::endl;

  } else {

    if (-offset_y > start_allocation1.get_height()) offset_y = -start_allocation1.get_height();
    if ( offset_y > start_allocation2.get_height()) offset_y =  start_allocation2.get_height();

    allocation1.set_height(start_allocation1.get_height() + offset_y);
    allocationh.set_y(     start_allocationh.get_y()      + offset_y);
    allocation2.set_y(     start_allocation2.get_y()      + offset_y);
    allocation2.set_height(start_allocation2.get_height() - offset_y);
  }

  queue_allocate(); // Relayout MyMultipaned content. Required.

  return true;
}

#endif // not USE_GESTURES
