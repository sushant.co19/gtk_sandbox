
/*
 * The main application.
 *
 * Copyright (C) 2020 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include "my-application.h"
#include "my-multipaned.h"

#include <iostream>

MyApplication::MyApplication()
  : Gtk::Application("org.tavmjong.application", Gio::APPLICATION_NON_UNIQUE)
{
  Glib::set_application_name("My Application");
}

Glib::RefPtr<MyApplication> MyApplication::create()
{
  return Glib::RefPtr<MyApplication>(new MyApplication());
}

void
MyApplication::on_startup()
{
  Gtk::Application::on_startup();
  std::cout << "MyApplication::on_startup()" << std::endl;
}

void
MyApplication::on_activate()
{
  std::cout << "MyApplication::on_activate()" << std::endl;

  auto provider = Gtk::CssProvider::create();
  try {
    provider->load_from_path("my-application.css");
  }
  catch (...) {
  }

  auto const screen = Gdk::Screen::get_default();
  Gtk::StyleContext::add_provider_for_screen (screen, provider, GTK_STYLE_PROVIDER_PRIORITY_USER);
 
  
  auto window = new Gtk::ApplicationWindow();
  add_window (*window);

  auto box = new Gtk::Box(Gtk::ORIENTATION_VERTICAL);
  window->add (*box);

  auto multipaned1 = new MyMultipaned();
  multipaned1->set_name ("Horizontal");
  box->add (*multipaned1);

  // auto box2 = Gtk::manage(new Gtk::Box());
  // for (int i = 0; i < 20; ++i) {
  //   auto label1 = new Gtk::Label( "A" );
  //   auto label2 = new Gtk::Label( "B" );
  //   label1->set_size_request(50,50);
  //   label1->set_name("A");
  //   label2->set_size_request(50,50);
  //   label2->set_name("B");
  //   box2->add(*label1);
  //   box2->add(*label2);
  // }
  // box->add(*box2);

  auto multipaned2 = new MyMultipaned(Gtk::ORIENTATION_VERTICAL);
  multipaned1->set_name ("Vertical");
  box->add (*multipaned2);

  for (int i = 0; i < 4; ++i) {
    auto label1 = new Gtk::Label( std::to_string(i) );
    auto label2 = new Gtk::Label( std::to_string(i) );
    label1->set_hexpand(true);
    label2->set_vexpand(true);
    multipaned1->prepend (label1);
    multipaned2->append  (label2);
  }

  window->show_all();
}
